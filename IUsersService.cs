﻿using Dutchupp.DAL.Models;
using Dutchupp.DAL.ViewModels;
using Dutchupp.DAL.ViewModels.Update;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dutchupp.Service.IRepository
{
    public interface IUsersService
    {
        (bool, string, User, string) InsertFacebookUser(FacebookLoginViewModel UserVM);
        (bool, string, User) UpdateUserInfo(UpdateUserProfileViewModel UserVM);
        (bool, string, User) GetUserInfo(int UserId, string access_token);

        (bool success, string message) SendMailVerification(SendEmailVerificationViewModel model);
        (bool success, string message) EmailVerification(EmailVerificationViewModel model);
        User GetUserById(int userId);
        User GetUserWithMediaById(int userId);
        string GetUserSocialId(int userId);
        string GetUserName(int userId);
        string GetUserEmail(int userId);
        string GetUserMobile(int userId);
        bool UsersExistById(int id);
        bool UsersExistByEmail(string email);
        bool UsersExistBySocialId(string SocialId);
        bool IsValidEmail(string email);
        bool IsValidAccessToken(int userId, string token);
        List<Images> GetUserImages(int userId);
        string GetBaseUrl();
    }
}
