﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Dutchupp.DAL.ViewModels
{
    public class UserViewModel
    {

        public Int32 UserId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Access token expired or enter a valid token.")]
        public string Access_token { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Social_id { get; set; }

        public int? Age { get; set; }
        public int? Gender { get; set; }

        //public string Password { get; set; }

        public string Mobile_number { get; set; }

        public string About { get; set; }

        public string Job_title { get; set; }

        public string Company { get; set; }

        public string School { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public string Profile_pic { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public int Activity_count { get; set; }
        public int Status { get; set; }
        public string Device_token { get; set; }
        public string Interests { get; set; }

        public List<Images> Media { get; set; }

    }

    public class Images
    {
        public int Id { get; set; }
        public string Media_url { get; set; }
        public int Media_type { get; set; }
    }
}
