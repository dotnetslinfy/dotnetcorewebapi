# Web APIs with ASP.NET Core 2.1 #

* This API project is in ASP.NET Core 2.1
* APIs are token based, we are generating token every time user logged-in and implements apis based on that access token. Services have the actual functionality we implemented which are following the repository DI pattern.

## APIs

### Login user with facebook

* We are saving the user information here & send success message with user data as api response.

### Get user Profile

* Api will send user data as response only when access token is valid.

### Update User Profile

* Update the user only when access token is valid or not expired as we generate new token on every login.

### Email Verification

* SendVerificationMail
 *Send Verification Mail

* VerificationStatus
 *Verify the user Status