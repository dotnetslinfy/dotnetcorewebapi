﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Dutchupp.DAL;
using Dutchupp.DAL.Models;
using Dutchupp.Service.Repository;
using Dutchupp.DAL.ViewModels;
using System.Dynamic;
using Dutchupp.Service.IRepository;
using Dutchupp.Service.Common;
using Dutchupp.DAL.ViewModels.Update;
using System.IO;

namespace Dutchupp.API.Controllers
{
    [Route("api/[controller]")]
    //[ApiController]
    public class DutchuppController : Controller
    {
        private readonly DutchuppContext _dbDutchupp;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IUsersService _usersService;

        public DutchuppController(DutchuppContext dbDutchupp,
            IHttpContextAccessor httpContextAccessor,
            IUsersService userService)
        {
            _dbDutchupp = dbDutchupp;
            _httpContextAccessor = httpContextAccessor;
            _usersService = userService;
        }

        [HttpPost("FacebookLogin")]
        public async Task<IActionResult> FacebookLogin([FromBody] FacebookLoginViewModel users)
        {
            if (!ModelState.IsValid)
            {
                return Ok(Helper.ModelStateError(ModelState));
            }

            (bool success, string message, User user, string token) = _usersService.InsertFacebookUser(users);

            dynamic Response = new ExpandoObject();
            Response.status = success;
            Response.message = message;
            Response.data = user;

            if (success) Response.access_token = token;

            return Ok(Response);        
        }

        [HttpGet("GetProfile")]
        public async Task<IActionResult> GetProfile([FromQuery] int user_id, string access_token)
        {
            (bool invalidParam, dynamic response) = Helper.ValidateParam(user_id, access_token);
            if (invalidParam)
                return Ok(response);

            (bool success, string message, User user) = _usersService.GetUserInfo(user_id, access_token);
            
            dynamic Response = new ExpandoObject();
            Response.status = success;
            Response.message = message;
            Response.data = user;

            return Ok(Response);
        }

        [HttpPost("UpdateProfile")]
        public async Task<IActionResult> UpdateProfile([FromBody] UpdateUserProfileViewModel user)
        {
            if (!ModelState.IsValid)
            {
                return Ok(Helper.ModelStateError(ModelState));
            }

            (bool success, string message, User _user) = _usersService.UpdateUserInfo(user);

            dynamic Response = new ExpandoObject();
            Response.status = success;
            Response.message = message;

            return Ok(Response);
        }

        #region Email Verification

        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpPost("SendVerificationMail")]
        public async Task<IActionResult> SendVerificationMail([FromBody] SendEmailVerificationViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Ok(Helper.ModelStateError(ModelState));
            }

            (bool success, string message) = _usersService.SendMailVerification(model);

            dynamic Response = new ExpandoObject();
            Response.status = success;
            Response.message = message;

            return Ok(Response); 
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpPost("VerificationStatus")]
        public async Task<IActionResult> VerificationStatus([FromBody] EmailVerificationViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Ok(Helper.ModelStateError(ModelState));
            }

            (bool success, string message) = _usersService.EmailVerification(model);

            dynamic Response = new ExpandoObject();
            Response.status = success;
            Response.message = message;

            return Ok(Response);
        }
        #endregion

    }
}