﻿using Dutchupp.DAL;
using Dutchupp.DAL.Models;
using Dutchupp.DAL.ViewModels;
using Dutchupp.Service.IRepository;
using Microsoft.EntityFrameworkCore;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using System.Net.Mail;
using Dutchupp.Service.Common;
using System.IO;
using static Dutchupp.Service.Common.Enums;
using Dutchupp.DAL.ViewModels.Update;
using Microsoft.AspNetCore.Http;

namespace Dutchupp.Service.Repository
{
    public class UsersService : IUsersService
    {
        private readonly DutchuppContext _dbDutchupp;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public UsersService(DutchuppContext context,
            IHttpContextAccessor httpContextAccessor)
        {
            _dbDutchupp = context;
            _httpContextAccessor = httpContextAccessor;
        }

        public (bool, string, User, string) InsertFacebookUser(FacebookLoginViewModel UserVM)
        {
            try
            {
                if (!string.IsNullOrEmpty(UserVM.Email))
                {
                    if (!IsValidEmail(UserVM.Email))
                        return (false, "Email address is not valid.", null, "");
                }

                string _accessToken = GenerateAccessToken(16).ToLower();
                
                var userExistWithSocialId = _dbDutchupp.Users.Include(m => m.Media).Where(u => u.Social_id == UserVM.Social_id.Trim()).FirstOrDefault();
                if (userExistWithSocialId != null)
                {
                    userExistWithSocialId.Access_token = _accessToken;
                    _dbDutchupp.SaveChanges();
                    return (true, "User login successfully.", userExistWithSocialId, _accessToken);
                }

                var _facebookUser = new User
                {
                    Name = UserVM.Name,
                    Email = UserVM.Email,
                    Age = UserVM.Age,
                    Mobile_number = UserVM.Mobile_number,
                    About = UserVM.About,
                    Interests = UserVM.Interests,
                    Gender = UserVM.Gender,
                    Profile_pic = UserVM.Image,
                    Device_token = UserVM.Device_token,
                    School = UserVM.School,
                    Job_title = UserVM.Job_title,
                    Company = UserVM.Company,
                    Status = (int)Status.NOT_VERIFIED,
                    Access_token = _accessToken,
                    Social_id = UserVM.Social_id,
                    CreatedOn = DateTime.Now
                };

                _dbDutchupp.Entry(_facebookUser).State = EntityState.Added;
                _dbDutchupp.SaveChanges();

                try
                {
                    var media = new Gallery();
                    media.UserId = _facebookUser.Id;
                    if (string.IsNullOrEmpty(_facebookUser.Profile_pic))
                        media.Media_url = _facebookUser.Profile_pic;
                    else
                        media.Media_url = _facebookUser.Profile_pic;

                    media.Media_type = 1;
                    media.CreatedOn = DateTime.Now;
                    _dbDutchupp.Add(media);
                    _dbDutchupp.SaveChanges();
                }
                catch { }
                return (true, "User login successfully.", _facebookUser, _accessToken);

            }
            catch (Exception ex)
            {
                Logger.Log(LogLevel.Error, ex.Message);
                return (false, ex.Message, null, "");
            }
           
        }

        public (bool, string, User) UpdateUserInfo(UpdateUserProfileViewModel UserVM)
        {
            try
            {
                if (UserVM.Access_token == null)
                    return (false, "Access token expired or enter a valid token.", null);

                var _user = _dbDutchupp.Users.Include(m => m.Media).Where(u => u.Id == UserVM.UserId).FirstOrDefault();
                if (_user == null) return (false, "User Not Found.", null);
                else if (_user.Access_token != UserVM.Access_token)
                    return (false, "Access token expired or enter a valid token.", null);


                _user.Name = UserVM.Name == null ? _user.Name : UserVM.Name;
                _user.Email = UserVM.Email == null ? _user.Email : UserVM.Email;
                _user.Age = UserVM.Age == null ? _user.Age : UserVM.Age;
                _user.Gender = UserVM.Gender == null ? _user.Gender : UserVM.Gender;
                _user.Mobile_number = UserVM.Mobile_number == null ? _user.Mobile_number : UserVM.Mobile_number;
                _user.About = UserVM.About == null ? _user.About : UserVM.About;
                _user.Job_title = UserVM.Job_title == null ? _user.Job_title : UserVM.Job_title;
                _user.Interests = UserVM.Interests == null ? _user.Interests : UserVM.Interests;
                _user.Company = UserVM.Company == null ? _user.Company : UserVM.Company;
                _user.School = UserVM.School == null ? _user.School : UserVM.School;
                _user.Profile_pic = UserVM.Profile_pic == null ? _user.Profile_pic : UserVM.Profile_pic;
                _user.Activity_count = UserVM.Activity_count == 0 ? _user.Activity_count : UserVM.Activity_count;

                _user.Status = UserVM.Status == null || UserVM.Status == 0 ? _user.Status : UserVM.Status;

                _dbDutchupp.SaveChanges();

                return (true, "User Succesfully Updated.", _user);
            }
            catch (Exception ex)
            {
                Logger.Log(LogLevel.Error, ex.Message);
                return (false, ex.Message, null);
            }
        }

        public (bool, string, User) GetUserInfo(int UserId, string access_token)
        {
            try
            {
                if (access_token == null)
                    return (false, "Access token expired or enter a valid token.", null);

                var _user = GetUserWithMediaById(UserId);
                if (_user == null) return (false, "User Not Found.", null);
                else if (_user.Access_token != access_token) return (false, "Access token expired or enter a valid token.", null);

                //var _userVM = Mapper.Map<UserViewModel>(_user);
                foreach (var media in _user.Media)
                {
                    if (media.Media_url == null) continue;
                    else if (media.Media_url.StartsWith("http")) continue;
                    media.Media_url = GetBaseUrl() + media.Media_url;
                }

                return (true, "", _user);
            }
            catch (Exception ex)
            {
                Logger.Log(LogLevel.Error, ex.Message);
                return (false, ex.Message, null);
            }
        }

        public (bool success, string message) SendMailVerification(SendEmailVerificationViewModel model)
        {
            try
            {
                var _user = _dbDutchupp.Users.Where(u => u.Id == model.UserId).FirstOrDefault();
                if (_user == null) return (false, "User email Not Found.");

                var pathHTML = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/HtmlTemplate/EmailVerificationHtml.html");
                var pathLOGO = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/HtmlTemplate/Image/logo.png");

                Random generator = new Random();
                String OTP = generator.Next(0, 999999).ToString("D6");

                _user.Verifi_token = OTP;
                _dbDutchupp.SaveChanges();

                var EmailContent = File.ReadAllText(pathHTML);
                EmailContent = EmailContent.Replace("IMAGE-LOGO", pathLOGO);
                EmailContent = EmailContent.Replace("NAME-TEXT", _user.Name); 
                EmailContent = EmailContent.Replace("OTP-TOKEN", OTP); 
                EmailContent = EmailContent.Replace("HIDDEN-LOGO", pathLOGO);

                var sendEmailViewModel = new SendEmailViewModel();
                sendEmailViewModel.Subject = "Dutchupp email verification";
                sendEmailViewModel.Body = EmailContent;
                sendEmailViewModel.IsHtml = true;
                sendEmailViewModel.ToMail = model.Email;

                if (Helper.SendEmail(sendEmailViewModel))
                {
                    return (true, "Verification mail has been sent successfully, please check your inbox.");
                }
                else
                    return (false, "Email has not been sent.");
            }
            catch (Exception ex)
            {
                Logger.Log(LogLevel.Error, ex.Message);
                return (false, ex.Message);
            }
        }

        public (bool success, string message) EmailVerification(EmailVerificationViewModel model)
        {
            try
            {
                var _user = _dbDutchupp.Users.Where(u => u.Id == model.UserId).FirstOrDefault();
                if (_user == null) return (false, "User Not Found.");
                else if (_user.Verifi_token == null) return (false, "OTP Not Found.");
                else if (_user.Verifi_token != model.OTP) return (false, "Please enter a valid OTP.");

                _user.Status = (int)Status.ACTIVE;
                _dbDutchupp.SaveChanges();
                
                var pathHTML = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/HtmlTemplate/VerifiedMailHtml.html");
                var pathLOGO = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/HtmlTemplate/Image/logo.png");
                
                var EmailContent = File.ReadAllText(pathHTML);
                EmailContent = EmailContent.Replace("IMAGE-LOGO", pathLOGO);
                EmailContent = EmailContent.Replace("NAME-TEXT", _user.Name);

                var sendEmailViewModel = new SendEmailViewModel();
                sendEmailViewModel.Subject = "Dutchupp | Your email has been verified";
                sendEmailViewModel.Body = EmailContent;
                sendEmailViewModel.IsHtml = true;
                sendEmailViewModel.ToMail = _user.Email;

                if (Helper.SendEmail(sendEmailViewModel))
                {
                    return (true, "Email has been verified.");
                }
                else return (true, "User verified. But email has not been sent.");

            }
            catch (Exception ex)
            {
                Logger.Log(LogLevel.Error, ex.Message);
                return (false, ex.Message);
            }
        }

        #region Common Method

        public User GetUserById(int userId)
        {
            return _dbDutchupp.Users.Where(u => u.Id == userId).FirstOrDefault();
        }

        public User GetUserWithMediaById(int userId)
        {
            return _dbDutchupp.Users.Include(z => z.Media).Where(u => u.Id == userId).FirstOrDefault();
        }

        public string GetUserName(int userId)
        {
            var user = _dbDutchupp.Users.Where(u => u.Id == userId).FirstOrDefault();
            return user == null ? "" : user.Name;
        }

        public string GetUserEmail(int userId)
        {
            var user = _dbDutchupp.Users.Where(u => u.Id == userId).FirstOrDefault();
            return user == null ? "" : user.Email;
        }

        public string GetUserMobile(int userId)
        {
            var user = _dbDutchupp.Users.Where(u => u.Id == userId).FirstOrDefault();
            return user == null ? "" : user.Mobile_number;
        }

        public string GetUserSocialId(int userId)
        {
            var user = _dbDutchupp.Users.Where(u => u.Id == userId).FirstOrDefault();
            return user == null ? "" : user.Social_id;
        }

        public bool UsersExistById(int id)
        {
            return _dbDutchupp.Users.Any(e => e.Id == id);
        }

        public bool UsersExistByEmail(string email)
        {
            return _dbDutchupp.Users.Any(e => e.Email == email);
        }

        public bool UsersExistBySocialId(string SocialId)
        {
            return _dbDutchupp.Users.Any(e => e.Social_id == SocialId);
        }

        public bool IsValidEmail(string email)
        {
            try
            {
                MailAddress m = new MailAddress(email);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        private string GenerateAccessToken(int length)
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public bool IsValidAccessToken(int userId, string token)
        {
            return _dbDutchupp.Users.Any(user => user.Id == userId && user.Access_token == token);
        }

        public List<Images> GetUserImages(int userId)
        {
            var imagesList = new List<Images>();
            var gallery = _dbDutchupp.Galleries.Where(u => u.UserId == userId).ToList();
            if (gallery == null) return imagesList;

            foreach (var img in gallery)
            {
                var image = new Images
                {
                    Id = img.Id,
                    Media_url = img.Media_url,
                    Media_type = img.Media_type
                };
                imagesList.Add(image);
            }

            return imagesList;
        }

        public string GetBaseUrl()
        {
            var _request = _httpContextAccessor.HttpContext.Request;
            var baseUrl = $"{_request.Scheme}://{_request.Host}{_request.PathBase}" + "/";
            return baseUrl;
        }
        #endregion
    }
}
